<?php
namespace AngularFilemanager\S3Bridge;

use Aws\Resource\Aws;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Illuminate\Support\Facades\Storage;

/**
 * File Manager API Class
 *
 * Made for PHP Local filesystem bridge for angular-filemanager to handle file manipulations
 * @author       Jakub Ďuraš <jakub@duras.me>
 */
class FileManagerApi
{

    protected $client, $bucket, $root;

    private $translate;

    public function __construct($config, $bucket, $lang = 'en', $muteErrors = false, $root = '')
    {
        $this->client = new S3Client($config);
        $this->bucket = $bucket;
        if ($muteErrors) {
            ini_set('display_errors', 0);
        }

        $this->root = rtrim($root, '/');

        if (!$this->client->doesBucketExist($this->bucket, false)) {
            throw new Exception("AWS S3 Bucket '{$this->bucket}' does not exist.");
        }
//        $this->translate = new Translate($lang);
    }

    public function postHandler($query, $request, $files)
    {
        $t = $this->translate;

        // Probably file upload
        if (!isset($request['action']) && (isset($_SERVER["CONTENT_TYPE"]) && strpos($_SERVER["CONTENT_TYPE"],
                    'multipart/form-data') !== false)
        ) {
            $uploaded = $this->uploadAction($request['destination'], $files);
            if ($uploaded === true) {
                $response = $this->simpleSuccessResponse();
            } else {
                $response = $this->simpleErrorResponse($t->upload_failed);
            }

            return $response;
        }

        switch ($request['action']) {

            case 'cropUpload':
                //upload new image
                $this->uploadCropImage($request['path'], $request['imageData'], $request['name']);

                //return listing of directory post upload
                $list = $this->listAction($request['path']);

                if (!is_array($list)) {
                    $response = $this->simpleErrorResponse($t->listing_filed);
                } else {
                    $response = new Response();
                    $response->setData([
                        'result' => $list
                    ]);
                }

                break;

            case 'search':
                $list = $this->searchAction($request['query'], $request['path']);

                if (!is_array($list)) {
                    $response = $this->simpleErrorResponse($t->listing_filed);
                } else {
                    $response = new Response();
                    $response->setData([
                        'result' => $list
                    ]);
                }
                break;

            case 'list':
                $list = $this->listAction($request['path']);

                if (!is_array($list)) {
                    $response = $this->simpleErrorResponse($t->listing_filed);
                } else {
                    $response = new Response();
                    $response->setData([
                        'result' => $list
                    ]);
                }
                break;

            case 'rename':
                $renamed = $this->renameAction($request['item'], $request['newItemPath']);
                if ($renamed === true) {
                    $response = $this->simpleSuccessResponse();
                } elseif ($renamed === 'notfound') {
                    $response = $this->simpleErrorResponse($t->file_not_found);
                } else {
                    $response = $this->simpleErrorResponse($t->renaming_failed);
                }
                break;

            case 'move':
                $moved = $this->moveAction($request['items'], $request['newPath']);
                if ($moved === true) {
                    $response = $this->simpleSuccessResponse();
                } else {
                    $response = $this->simpleErrorResponse($t->moving_failed);
                }
                break;

            case 'copy':
                if (isset($request['singleFilename'])) {
                    $filename = $request['singleFilename'];
                } else {
                    $filename = false;
                }
                $copied = $this->copyAction($request['items'], $request['newPath'], $filename);
                if ($copied === true) {
                    $response = $this->simpleSuccessResponse();
                } else {
                    $response = $this->simpleErrorResponse($t->copying_failed);
                }
                break;

            case 'remove':
                $removed = $this->removeAction($request['items']);
                if ($removed === true) {
                    $response = $this->simpleSuccessResponse();
                } elseif ($removed === 'notempty') {
                    $response = $this->simpleErrorResponse($t->removing_failed_directory_not_empty);
                } else {
                    $response = $this->simpleErrorResponse($t->removing_failed);
                }
                break;

            case 'edit':
                $edited = $this->editAction($request['item'], $request['content']);
                if ($edited !== false) {
                    $response = $this->simpleSuccessResponse();
                } else {
                    $response = $this->simpleErrorResponse($t->saving_failed);
                }
                break;

            case 'getContent':
                $content = $this->getContentAction($request['item']);
                if ($content !== false) {
                    $response = new Response();
                    $response->setData([
                        'result' => $content
                    ]);
                } else {
                    $response = $this->simpleErrorResponse($t->file_not_found);
                }
                break;

            case 'createFolder':
                $created = $this->createFolderAction($request['newPath']);
                if ($created === true) {
                    $response = $this->simpleSuccessResponse();
                } elseif ($created === 'exists') {
                    $response = $this->simpleErrorResponse($t->folder_already_exists);
                } else {
                    $response = $this->simpleErrorResponse($t->folder_creation_failed);
                }
                break;

            case 'changePermissions':
                $changed = $this->changePermissionsAction($request['items'], $request['perms'], $request['recursive']);
                if ($changed === true) {
                    $response = $this->simpleSuccessResponse();
                } elseif ($changed === 'missing') {
                    $response = $this->simpleErrorResponse($t->file_not_found);
                } else {
                    $response = $this->simpleErrorResponse($t->permissions_change_failed);
                }
                break;

            case 'compress':
                $compressed = $this->compressAction($request['items'], $request['destination'],
                    $request['compressedFilename']);
                if ($compressed === true) {
                    $response = $this->simpleSuccessResponse();
                } else {
                    $response = $this->simpleErrorResponse($t->compression_failed);
                }
                break;

            case 'extract':
                $extracted = $this->extractAction($request['destination'], $request['item'], $request['folderName']);
                if ($extracted === true) {
                    $response = $this->simpleSuccessResponse();
                } elseif ($extracted === 'unsupported') {
                    $response = $this->simpleErrorResponse($t->archive_opening_failed);
                } else {
                    $response = $this->simpleErrorResponse($t->extraction_failed);
                }
                break;

            default:
                $response = $this->simpleErrorResponse($t->function_not_implemented);
                break;
        }

        return $response;
    }

    public function getHandler($queries)
    {
        $t = $this->translate;

        switch ($queries['action']) {
            case 'download':
                $downloaded = $this->downloadAction($queries['path']);
                if ($downloaded === true) {
                    exit;
                } else {
                    $response = $this->simpleErrorResponse($t->file_not_found);
                }

                break;

            default:
                $response = $this->simpleErrorResponse($t->function_not_implemented);
                break;
        }

        return $response;
    }

    public function getObjectUrl($key)
    {
        return "https://s3.amazonaws.com/{$this->bucket}/{$this->root}/{$key}"; //$this->client->getObjectUrl($this->bucket, $key)
    }

    public function getObjectThumbnail($key)
    {
        return false;

        //Default here will just check file name for image extensions ...
        $thumbnail_extensions = array(
            'jpg',
            'jpeg',
            'png',
            'gif',
        );

        $info = pathinfo( $this->getObjectUrl( $key ) );
        //Need to remove the first directory
        $parts = explode( "/", $key );
        $first = true;
        $full_key = "";
        foreach( $parts as $part ) {
            if( $first )
            {
                $first = false;
                continue;
            }
            $full_key .= $part . '/';
        }

        $full_key = rtrim($full_key, " /" );

        if( in_array( $info['extension'], $thumbnail_extensions ) ) {
            //Example URL this would need to be overwritten, but everything should be overwritten in the SC Controller App
            return "http://rbjimmy.pomonashul.shuls.rustybrick.net/_preview/250/250/" . $full_key . "&foce=1&background=ffffff";
        }


        return false;
    }

    protected function downloadAction($path)
    {
        //redirect (302) get object url

        $key = ltrim($path, "/ ");
        $full_path = $this->getObjectUrl(ltrim($path, "/ "));

        $full_key = $this->root . '/' . $key;

        if (!$this->client->doesObjectExist($this->bucket, $full_key)) {
            return false;
        }

        $file_parts = explode("/", $path);
        $filename = $file_parts[count($file_parts) - 1];
//        header('Location: ' . $full_path, true, 302);




        header('Cache-Control: must-revalidate');
        header('Pragma: public');
//        header('Content-Length: ' . filesize($full_path) );
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        echo Storage::disk('s3')->get( $full_key );

        return true;
    }

    protected function uploadAction($path, $files, $overwrite = false, $type = null)
    {
        $path = ltrim($path, "/ ");
        foreach ($files as $name) {

            $key = $this->root . '/' . $path . '/' . $name['name'];

            if (!$overwrite && $this->client->doesObjectExist($this->bucket, $key)) {
                return array('error' => 'key_exists');
            }

            $params = array(
                'Bucket'     => $this->bucket,
                'Key'        => $key,
                'ACL'        => 'public-read',
                'SourceFile' => $name['tmp_name'],
                'ContentMD5' => '',
            );
            if ($name['type']) {
                $params['ContentType'] = $name['type'];
            }


            $result = $this->client->putObject($params);

        }

        return true;

    }

    protected function uploadCropImage($path, $imageData, $name, $overwrite = false)
    {
        preg_match_all('/data:image\/(gif|jpeg|png);base64,(.*)/', $imageData,
            $matches); //from the raw data, get the info

        if (in_array(trim($matches[1][0]), array("jpeg", "jpg", "gif", "png"))) { //check to see what type
            $key_parts = explode('.', $name); //remove an extension if they put one
            $filename = preg_replace("/[^a-z0-9\.]/", "", strtolower($key_parts[0]));
            $key = $filename . '.jpg';
        } else {
            return false;       // no image data to store
        }

        $key = $this->root . ltrim($path, "/ ") . '/' . $key;
        if (!$overwrite && $this->client->doesObjectExist($this->bucket, $key)) {
            return array('error' => 'key_exists');
        }

        $params = array(
            'Bucket'     => $this->bucket,
            'Key'        => $key,
            'ACL'        => 'public-read',
            'ContentMD5' => '',
            'Body'       => base64_decode($matches[2][0]),
        );

//        if ($name['type']) {
//            $params['ContentType'] = $name['type'];
//        }

        $result = $this->client->putObject($params);

        return true;
    }


    protected function searchAction($query = '', $prefix = '', $recursive = true)
    {

        $prefix = ltrim($prefix, "/ ");

        $objects = array(
            'path'    => $prefix,
            'folders' => array(),
            'files'   => array()
        );

        if ($prefix != '') {
            $prefix = '/' . $prefix;
        }

        $prefix = $this->root . $prefix;

        do {
            $result = $this->client->listObjects(array(
                'Marker'    => isset($result) ? $result['NextMarker'] : '',
                'Bucket'    => $this->bucket,
                'Delimiter' => $recursive ? '' : '/',
                'Prefix'    => $prefix == '' ? '' : $prefix . '/',
            ));

            foreach ($result['Contents'] ?: array() as $file) {
                if ($file['Key'] === $prefix) {
                    continue;
                }

                $original_key = $file['Key'];

                $key_parts = explode("/", $original_key);

                if (!stristr($key_parts[count($key_parts) - 1], $query)) {
                    continue;
                }

                $short_key = str_replace_first($prefix == '' ? '' : $prefix . '/', '', $file['Key']);
                $get_params = ['Key' => $original_key, 'Bucket' => $this->bucket];

                $objects['files'][] = array(
                    'name'   => preg_replace('/^' . preg_quote($this->root, '/') . '/', '', $short_key),
                    'size'   => $file['Size'],
//                    'last_modified' => date( 'Y-m-d H:i:s', strtotime( $file['LastModified'] )),
//                    'url' => $this->client->getObject( $get_params ),
                    'url'    => $this->getObjectUrl($original_key),
                    'date'   => date('Y-m-d H:i:s', strtotime($file['LastModified'])),
                    'type'   => 'file',
                    'rights' => '',
                );
            }

//            foreach ($result['CommonPrefixes'] ?: array() as $subfolder) {
//
//                $subfolder['Prefix'] = str_replace($prefix == '' ? '' : $prefix . '/', '',
//                    rtrim($subfolder['Prefix'], "/ "));
//
//                if (trim($subfolder['Prefix']) == "") {
//                    continue;
//                }
//
//                $objects['files'][] =
//                    [
//                        'name'   => preg_replace('/^' . preg_quote($this->root, '/') . '/', '', $subfolder['Prefix']),
////                        'size' => $subfolder['Size'],
////                        'last_modified' => null,
////                        'url' => $this->getObjectUrl($subfolder['Key']),
//                        'date'   => null,
//                        'type'   => 'dir',
//                        'rights' => '',
//                ];
//            }
        } while ($result['IsTruncated']);

        return $objects['files'];
    }

    protected function listAction($prefix = '', $recursive = false)
    {

        $prefix = ltrim($prefix, "/ ");

        $objects = array(
            'path'    => $prefix,
            'folders' => array(),
            'files'   => array()
        );

        if ($prefix != '') {
            $prefix = '/' . $prefix;
        }

        $prefix = $this->root . $prefix;


        do {
            $result = $this->client->listObjects(array(
                'Marker'    => isset($result) ? $result['NextMarker'] : '',
                'Bucket'    => $this->bucket,
                'Delimiter' => $recursive ? '' : '/',
                'Prefix'    => $prefix == '' ? '' : $prefix . '/',
            ));

            foreach ($result['Contents'] ?: array() as $file) {

                if ($file['Key'] === $prefix || $file['Key'] === $prefix . '/') {
                    continue;
                }

                $original_key = $file['Key'];
                $file['Key'] = str_replace($prefix == '' ? '' : $prefix . '/', '', $file['Key']);
                $get_params = ['Key' => $original_key, 'Bucket' => $this->bucket];

                $object_thumbnail = $this->getObjectThumbnail($original_key);

                $objects['files'][] = array(
                    'name'      => preg_replace('/^' . preg_quote($this->root, '/') . '/', '', $file['Key']),
                    'size'      => $file['Size'],
//                    'last_modified' => date( 'Y-m-d H:i:s', strtotime( $file['LastModified'] )),
//                    'url' => $this->client->getObject( $get_params ),
                    'url'       => $this->getObjectUrl($file['Key']),
                    'thumbnail' => $object_thumbnail,
                    'date'      => date('Y-m-d H:i:s', strtotime($file['LastModified'])),
                    'type'      => $object_thumbnail ? 'preview_file' : 'file',
                    'rights'    => '',
                );
            }

            foreach ($result['CommonPrefixes'] ?: array() as $subfolder) {

                $subfolder['Prefix'] = str_replace($prefix == '' ? '' : $prefix . '/', '',
                    rtrim($subfolder['Prefix'], "/ "));

                if (trim($subfolder['Prefix']) == "") {
                    continue;
                }

                $objects['files'][] =
                    [
                        'name'   => preg_replace('/^' . preg_quote($this->root, '/') . '/', '', $subfolder['Prefix']),
//                        'size' => $subfolder['Size'],
//                        'last_modified' => null,
//                        'url' => $this->getObjectUrl($subfolder['Key']),
                        'date'   => null,
                        'type'   => 'dir',
                        'rights' => '',
                ];
            }
        } while ($result['IsTruncated']);

        return $objects['files'];

//        $objects = $this->s3->getIterator('ListObjects', ['Bucket' => $this->bucket] );
//        foreach( $objects as $object )
//        {
//            dd( $object );
//
//        }
//
//        $files = array_map(function($file){
//
//            $date = new \DateTime('@' . filemtime($file));
//            return [
//                'name' => utf8_encode(basename($file)),
//                'rights' => $this->parsePerms(fileperms($file)),
//                'size' => filesize($file),
//                'date' => $date->format('Y-m-d H:i:s'),
//                'type' => is_dir($file) ? 'dir' : 'file'
//            ];
//        }, $files);
//
//        return $files;
    }

    protected function renameAction($oldPath, $newPath)
    {
        if (!is_array($oldPath)) {
            $oldPath = array($oldPath);
        }

        foreach ($oldPath as $original_path) {

            $original_path = ltrim($original_path, "/ ");
            $newPath = ltrim($newPath, "/ ");

            if (substr($newPath, - 1) == '/') {
                $new_filename = $newPath . basename($original_path);
            } else {
                $new_filename = $newPath;
            }

            //Copy and then delete original
            try {
                $result = $this->client->copyObject([
                    'Bucket'     => $this->bucket,
                    'CopySource' => $this->bucket . '/' . $original_path,
                    'Key'        => $new_filename,
                    'ACL'        => 'public-read',
                ]);
            } catch (S3Exception $e) {
                dd($e);
            }

            $this->removeAction($original_path);
        }

        return true;

//        if (!file_exists($oldPath)) {
//            return 'notfound';
//        }
//
//        return rename($oldPath, $newPath);
    }

    protected function moveAction($oldPaths, $newPath)
    {
        return $this->renameAction($oldPaths, $newPath . '/');

//        $newPath = $this->basePath . rtrim($newPath, '/') . '/';
//
//        foreach ($oldPaths as $oldPath) {
//            if (!file_exists($this->basePath . $oldPath)) {
//                return false;
//            }
//
//            $renamed = rename($this->basePath . $oldPath, $newPath . basename($oldPath));
//            if ($renamed === false) {
//                return false;
//            }
//        }
//
//        return true;
    }

    protected function copyAction($oldPaths, $newPath, $newBaseName = false)
    {
        $newPath = $this->basePath . rtrim($newPath, '/') . '/';

        foreach ($oldPaths as $oldPath) {

            $oldPath = ltrim($oldPath, "/ ");

            if (!$this->client->doesObjectExist($this->bucket, $oldPath)) {
                return false;
            }

            $oldPath = ltrim($oldPath, "/ ");
            $newPath = ltrim($newPath, "/ ");

            if (substr($newPath, - 1) == '/') {
                if ($newBaseName) {
                    $new_filename = $newPath . $newBaseName;
                } else {
                    $new_filename = $newPath . basename($oldPath);
                }

            } else {
                $new_filename = $newPath;
            }

            $result = $this->client->copyObject([
                'Bucket'     => $this->bucket,
                'CopySource' => $this->bucket . '/' . $oldPath,
                'Key'        => $new_filename,
                'ACL'        => 'public-read',
            ]);

//            $copied = copy(
//                $this->basePath . $oldPath,
//                $newPath . basename($oldPath)
//            );
//            if ($copied === false) {
//                return false;
//            }
        }

        return true;
    }

    protected function removeAction($keys)
    {
        if (!$keys) {
            return false;
        }

        if (!is_array($keys)) {
            $keys = array($keys);
        }

        $root = $this->root;
        $objects = array_map(function ($key) use ($root) {
            return array('Key' => $root . $key);
        }, $keys);

        $deleted = 0;
        foreach (array_chunk($objects, 1000) as $chunk) {

            foreach ($chunk as $key => $fileinfo) {
                $chunk[$key]['Key'] = ltrim($fileinfo['Key'], "/ ");
            }

            try {
                $result = $this->client->deleteObjects(array(
                    'Bucket' => $this->bucket,
                    'Delete' =>
                        [
                            'Objects' => $chunk,
                        ],
                ));

                $deleted += count($result['Deleted']);
            } catch (S3Exception $e) {
                dd($e);
            }
        }

        if ($deleted) {
            return true;
        } else {
            return false;
        }

    }

    protected function editAction($path, $content)
    {
        $path = $this->basePath . $path;

        return file_put_contents($path, $content);
    }

    protected function getContentAction($path)
    {
        $path = $this->basePath . $path;

        if (!file_exists($path)) {
            return false;
        }

        return file_get_contents($path);
    }

    protected function createFolderAction($path)
    {
        $path = ltrim($path, "/ ");
        $key = $this->root . '/' . $path . '/';

        $result = $this->client->putObject(array(
            'Bucket' => $this->bucket,
            'Key'    => $key,
            'Body'   => '',
        ));

        return true;

    }

    protected function changePermissionsAction($paths, $permissions, $recursive)
    {
        foreach ($paths as $path) {
            if (!file_exists($this->basePath . $path)) {
                return 'missing';
            }

            if (is_dir($path) && $recursive === true) {
                $iterator = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($path),
                    RecursiveIteratorIterator::SELF_FIRST
                );

                foreach ($iterator as $item) {
                    $changed = chmod($this->basePath . $item, octdec($permissions));

                    if ($changed === false) {
                        return false;
                    }
                }
            }

            return chmod($this->basePath . $path, octdec($permissions));
        }
    }

    protected function compressAction($paths, $destination, $archiveName)
    {
        $archivePath = $this->basePath . $destination . $archiveName;

        $zip = new ZipArchive();
        if ($zip->open($archivePath, ZipArchive::CREATE) !== true) {
            return false;
        }

        foreach ($paths as $path) {
            $zip->addFile($this->basePath . $path, basename($path));
        }

        return $zip->close();
    }

    protected function extractAction($destination, $archivePath, $folderName)
    {
        $archivePath = $this->basePath . $archivePath;
        $folderPath = $this->basePath . rtrim($destination, '/') . '/' . $folderName;

        $zip = new ZipArchive;
        if ($zip->open($archivePath) === false) {
            return 'unsupported';
        }

        mkdir($folderPath);
        $zip->extractTo($folderPath);

        return $zip->close();
    }

    protected function simpleSuccessResponse()
    {
        $response = new Response();
        $response->setData([
            'result' => [
                'success' => true
            ]
        ]);

        return $response;
    }

    protected function simpleErrorResponse($message)
    {
        $response = new Response();
        $response
            ->setStatus(500, 'Internal Server Error')
            ->setData([
                'result' => [
                    'success' => false,
                    'error'   => $message
                ]
            ]);

        return $response;
    }

    protected function parsePerms($perms)
    {
        if (($perms & 0xC000) == 0xC000) {
            // Socket
            $info = 's';
        } elseif (($perms & 0xA000) == 0xA000) {
            // Symbolic Link
            $info = 'l';
        } elseif (($perms & 0x8000) == 0x8000) {
            // Regular
            $info = '-';
        } elseif (($perms & 0x6000) == 0x6000) {
            // Block special
            $info = 'b';
        } elseif (($perms & 0x4000) == 0x4000) {
            // Directory
            $info = 'd';
        } elseif (($perms & 0x2000) == 0x2000) {
            // Character special
            $info = 'c';
        } elseif (($perms & 0x1000) == 0x1000) {
            // FIFO pipe
            $info = 'p';
        } else {
            // Unknown
            $info = 'u';
        }

        // Owner
        $info .= (($perms & 0x0100) ? 'r' : '-');
        $info .= (($perms & 0x0080) ? 'w' : '-');
        $info .= (($perms & 0x0040) ?
            (($perms & 0x0800) ? 's' : 'x') :
            (($perms & 0x0800) ? 'S' : '-'));

        // Group
        $info .= (($perms & 0x0020) ? 'r' : '-');
        $info .= (($perms & 0x0010) ? 'w' : '-');
        $info .= (($perms & 0x0008) ?
            (($perms & 0x0400) ? 's' : 'x') :
            (($perms & 0x0400) ? 'S' : '-'));

        // World
        $info .= (($perms & 0x0004) ? 'r' : '-');
        $info .= (($perms & 0x0002) ? 'w' : '-');
        $info .= (($perms & 0x0001) ?
            (($perms & 0x0200) ? 't' : 'x') :
            (($perms & 0x0200) ? 'T' : '-'));

        return $info;
    }
}

function str_replace_first($from, $to, $subject)
{
    $from = '/' . preg_quote($from, '/') . '/';

    return preg_replace($from, $to, $subject, 1);
}
